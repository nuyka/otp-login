package com.nuyka.assignment.android.setup.base.activity;

import android.os.Bundle;
import android.view.Gravity;
import android.widget.Toast;

import com.nuyka.assignment.android.setup.contracts.ToastWrapper;
import com.nuyka.assignment.android.setup.factory.ViewModelFactory;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import dagger.android.support.DaggerAppCompatActivity;

public abstract class BaseInjectorActivity extends DaggerAppCompatActivity implements ToastWrapper {

    @Nullable
    @Inject
    protected ViewModelFactory viewModelFactory;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setActivityLayout());
    }

    protected abstract int setActivityLayout();

    @Override
    public void showMessage(String message) {
        Toast toast = Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }
}
