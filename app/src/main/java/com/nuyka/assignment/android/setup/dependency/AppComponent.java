package com.nuyka.assignment.android.setup.dependency;

import android.app.Application;

import com.nuyka.assignment.android.setup.dependency.modules.AppModule;
import com.nuyka.assignment.android.setup.dependency.modules.ResourceModule;
import com.nuyka.assignment.android.setup.dependency.modules.UseCaseModule;
import com.nuyka.assignment.android.setup.dependency.modules.VMModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class, UseCaseModule.class, AppModule.class,
        VMModule.class, ViewBuilder.class, ResourceModule.class})
public interface AppComponent extends AndroidInjector<DaggerApplication> {

    void inject(ApplicationClass applicationClass);

    @Override
    void inject(DaggerApplication instance);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

}
