package com.nuyka.assignment.android.screens.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nuyka.assignment.android.R;
import com.nuyka.assignment.android.screens.login.LoginActivity;
import com.nuyka.assignment.android.setup.base.activity.BaseInjectorActivity;
import com.nuyka.assignment.android.setup.contracts.PreferenceWrapper;
import com.nuyka.assignment.android.setup.contracts.ResourceMapper;
import com.nuyka.assignment.android.utils.Constants;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityOptionsCompat;

public class HomeActivity extends BaseInjectorActivity {

    private TextView headerView;
    private ImageView viewAppLogoImage;

    @Inject
    ResourceMapper resourceMapper;

    @Inject
    PreferenceWrapper preferenceWrapper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        headerView = findViewById(R.id.view_text_login_header);
        headerView.setText(resourceMapper.getString(R.string.header_home));
        viewAppLogoImage = findViewById(R.id.view_image_app_logo);
    }

    @Override
    protected int setActivityLayout() {
        return R.layout.home;
    }

    public void onLogoutClick(View view) {
        showLogoutWarning();
    }

    private void showLogoutWarning() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.logout_warning, null);
        dialogBuilder.setView(dialogView);

        dialogView.findViewById(R.id.view_image_logout).setOnClickListener(v -> {
            preferenceWrapper.putBoolean(Constants.IS_USER_LOGIN, false);
            ActivityOptionsCompat activityOptionsCompat =
                    ActivityOptionsCompat.makeSceneTransitionAnimation(this,viewAppLogoImage,resourceMapper.getString(R.string.image_logo_transition));
            Intent in = new Intent(this, LoginActivity.class);
            startActivity(in,activityOptionsCompat.toBundle());
            finish();
        });
        dialogBuilder.show();
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }
}
