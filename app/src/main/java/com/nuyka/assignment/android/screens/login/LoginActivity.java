package com.nuyka.assignment.android.screens.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.nuyka.assignment.android.R;
import com.nuyka.assignment.android.model.Country;
import com.nuyka.assignment.android.screens.country.CountrySearchActivity;
import com.nuyka.assignment.android.screens.home.HomeActivity;
import com.nuyka.assignment.android.screens.login.sendotp.SendOtpFragment;
import com.nuyka.assignment.android.screens.login.sendotp.SendOtpViewModel;
import com.nuyka.assignment.android.screens.login.validate.ValidateOtpFragment;
import com.nuyka.assignment.android.screens.login.validate.ValidateOtpViewModel;
import com.nuyka.assignment.android.setup.base.activity.BaseInjectorActivity;
import com.nuyka.assignment.android.setup.contracts.PreferenceWrapper;
import com.nuyka.assignment.android.setup.contracts.ResourceMapper;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import static com.nuyka.assignment.android.utils.Constants.IS_USER_LOGIN;
import static com.nuyka.assignment.android.utils.Constants.RESENT_OTP_REQUEST;
import static com.nuyka.assignment.android.utils.Constants.SEND_OTP_REQUEST;

public class LoginActivity extends BaseInjectorActivity {

    private final String TAG = LoginActivity.class.getName();
    private static final int SEARCH_REQ_CODE = 101;

    private SendOtpViewModel sendOtpViewModel;
    private ValidateOtpViewModel validateOtpViewModel;

    private ImageView viewAppLogoImage;
    private ImageView viewBannerImage;
    private TextView headerView;
    private TextView heyThereView;

    private FirebaseAuth mAuth;
    private int OTP_REQUEST_MAKER;

    @Inject
    ResourceMapper resourceMapper;

    @Inject
    PreferenceWrapper preferenceWrapper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();

        registerViews();
        registerViewModels();
        registerEventListener();
        addOnFrame(SendOtpFragment.getInstance(), SendOtpFragment.class.getName());
    }

    private void registerViews() {
        viewAppLogoImage = findViewById(R.id.view_image_app_logo);
        viewBannerImage = findViewById(R.id.banner_image);
        heyThereView = findViewById(R.id.view_text_header);
        headerView = findViewById(R.id.view_text_login_header);
        headerView.setText(resourceMapper.getString(R.string.header_login));
    }

    private void registerEventListener() {
        setOtpEventListener();
    }

    private void setOtpEventListener() {
        sendOtpViewModel.getSendOtpEvents().observeEvent(this, otpstates -> {
            switch (otpstates) {
                case OTP_TO_BE_SENT:
                    OTP_REQUEST_MAKER = SEND_OTP_REQUEST;
                    if (validateOtpViewModel.getToken() != null)
                        resendOtpOnMobile(sendOtpViewModel.getPhoneNumber());
                    else
                        sendOtpOnMobile(sendOtpViewModel.getPhoneNumber());
                    break;
                case OTP_SENT:
                    validateOtpViewModel.reset();
                    viewBannerImage.setBackgroundResource(R.drawable.img_enter_otp);
                    addOnFrame(ValidateOtpFragment.getInstance(),ValidateOtpFragment.class.getName());
                    break;
                case SELECT_CODE:
                    ActivityOptionsCompat activityOptionsCompat =
                            ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                                    Pair.create(heyThereView,resourceMapper.getString(R.string.view_title_transition)),
                                    Pair.create(viewBannerImage,resourceMapper.getString(R.string.image_banner_transition)));
                    Intent in = new Intent(this, CountrySearchActivity.class);
                    startActivityForResult(in,SEARCH_REQ_CODE,activityOptionsCompat.toBundle());
                    break;
            }
        });

        validateOtpViewModel.getValidateOtpEvents().observeEvent(this, otpstates -> {
            switch (otpstates) {
                case OTP_TO_BE_VALIDATE:
                    verifyOtp();
                    break;
                case CHANGE_NUMBER:
                    getSupportFragmentManager().popBackStack();
                    viewBannerImage.setBackgroundResource(R.drawable.img_enter_otp);
                    break;
                case OTP_RESEND:
                    OTP_REQUEST_MAKER = RESENT_OTP_REQUEST;
                    resendOtpOnMobile(sendOtpViewModel.getPhoneNumber());
                    break;
                case OTP_VALID:
                    preferenceWrapper.putBoolean(IS_USER_LOGIN, true);
                    ActivityOptionsCompat activityOptionsCompat =
                            ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                                    Pair.create(viewAppLogoImage,resourceMapper.getString(R.string.image_logo_transition)),
                                    Pair.create(viewBannerImage,resourceMapper.getString(R.string.image_banner_transition)));
                    Intent in = new Intent(this, HomeActivity.class);
                    startActivity(in,activityOptionsCompat.toBundle());
                    break;
            }
        });
    }

    private void sendOtpOnMobile(String phoneNumber) {
        Log.e(TAG, "otp sent to: " + phoneNumber);
        PhoneAuthOptions options = PhoneAuthOptions.newBuilder(mAuth)
                .setPhoneNumber(phoneNumber)       // Phone number to verify
                .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                .setActivity(this)                 // Activity (for callback binding)
                .setCallbacks(mCallbacks)          // OnVerificationStateChangedCallbacks
                .build();
        PhoneAuthProvider.verifyPhoneNumber(options);
    }

    private void resendOtpOnMobile(String phoneNumber) {
        Log.e(TAG, "otp sent to: " + phoneNumber);
        PhoneAuthOptions options =
                PhoneAuthOptions.newBuilder(mAuth)
                        .setPhoneNumber(phoneNumber)       // Phone number to verify
                        .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                        .setActivity(this)                 // Activity (for callback binding)
                        .setCallbacks(mCallbacks)          // OnVerificationStateChangedCallbacks
                        .setForceResendingToken(validateOtpViewModel.getToken())     // ForceResendingToken from callbacks
                        .build();
        PhoneAuthProvider.verifyPhoneNumber(options);
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            Log.e(TAG,"onVerificationCompleted: " + phoneAuthCredential);
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            Log.e(TAG,"onVerificationFailed: " + e.getMessage());
            switch (OTP_REQUEST_MAKER) {
                case SEND_OTP_REQUEST:
                    sendOtpViewModel.otpSendingFailed();
                    break;
                case RESENT_OTP_REQUEST:
                    validateOtpViewModel.otpSendingFailed();
                    break;
            }
            showMessage(String.format(resourceMapper.getString(R.string.otp_not_sent),sendOtpViewModel.getPhoneNumber()));
        }

        @Override
        public void onCodeSent(@NonNull String verificationId, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            Log.e(TAG,"onCodeSent: verificationId - " + verificationId);
            validateOtpViewModel.setVerificationId(verificationId);
            validateOtpViewModel.setResendToken(forceResendingToken);
            startSMSListener();
            switch (OTP_REQUEST_MAKER) {
                case SEND_OTP_REQUEST:
                    sendOtpViewModel.otpSent();
                    break;
                case RESENT_OTP_REQUEST:
                    validateOtpViewModel.otpResent();
                    showMessage(String.format(resourceMapper.getString(R.string.otp_sent),sendOtpViewModel.getPhoneNumber()));
                    break;
            }
        }
    };

    private void verifyOtp() {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(
                validateOtpViewModel.getVerificationId(),
                validateOtpViewModel.getCode()
        );
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, task -> {
            if (task.isSuccessful()) {
                Log.e(TAG,"otp valid");
                validateOtpViewModel.otpValidated();
            } else {
                Log.e(TAG,"otp invalid");
                validateOtpViewModel.otpInvalid();
            }
        });
    }

    public void startSMSListener() {
        SmsRetrieverClient mClient = SmsRetriever.getClient(this);
        Task<Void> mTask = mClient.startSmsRetriever();
        mTask.addOnSuccessListener(aVoid -> Log.e(TAG, "sms retriver started"));
        mTask.addOnFailureListener(e -> Log.e(TAG, "sms retriver failed"));
    }

    private void registerViewModels() {
        sendOtpViewModel = new ViewModelProvider(this, viewModelFactory).get(SendOtpViewModel.class);
        validateOtpViewModel = new ViewModelProvider(this, viewModelFactory).get(ValidateOtpViewModel.class);
    }

    private void addOnFrame(Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in, R.anim.fade_out)
                .add(R.id.login_frame, fragment, tag)
                .addToBackStack(tag)
                .commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SEARCH_REQ_CODE) {
            if (resultCode == RESULT_OK) {
                Country selectedCountry = (Country) data.getSerializableExtra("country");
                Log.e(TAG,"onActivityResult : " + selectedCountry.getName());
                SendOtpFragment sendOtpFragment = (SendOtpFragment)getSupportFragmentManager().findFragmentByTag(SendOtpFragment.class.getName());
                if (sendOtpFragment != null) {
                    sendOtpFragment.resetCountryFormat(selectedCountry);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

    @Override
    protected int setActivityLayout() {
        return R.layout.login;
    }
}
