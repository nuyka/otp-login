package com.nuyka.assignment.android.setup.contracts;

public interface PreferenceWrapper {

    void putInt(String key, int value);
    void putString(String key, String value);
    void putFloat(String key, float value);
    void putLong(String key, long value);
    void putBoolean(String key, boolean value);
    int getInt(String key, int defValue);
    String getString(String key, String defValue);
    float getFloat(String key, float defValue);
    long getLong(String key, long defValue);
    boolean getBoolean(String key, boolean defValue);
    void clear();
}
