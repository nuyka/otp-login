package com.nuyka.assignment.android.setup.contracts;

public interface OtpReceivedInterface {
    void onOtpReceived(String otp);

    void onOtpTimeout();
}