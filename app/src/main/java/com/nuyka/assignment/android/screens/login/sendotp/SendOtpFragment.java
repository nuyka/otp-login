package com.nuyka.assignment.android.screens.login.sendotp;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.nuyka.assignment.android.databinding.SendOtpBinding;
import com.nuyka.assignment.android.model.Country;
import com.nuyka.assignment.android.setup.base.fragment.BaseInjectorFragment;
import com.nuyka.assignment.android.utils.PhoneNumberHelper;

import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import me.ibrahimsn.lib.PhoneNumberKit;

import static android.app.Activity.RESULT_OK;

@SuppressWarnings("deprecation")
public class SendOtpFragment extends BaseInjectorFragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private final String TAG = SendOtpFragment.class.getName();

    private SendOtpViewModel viewModel;
    private SendOtpBinding viewBinding;
    private TextInputEditText phoneNumberEditView;
    private TextInputLayout phoneNumberEditViewInput;
    private String countryIso;
    private GoogleApiClient mGoogleApiClient;
    private final int RESOLVE_HINT = 2;

    public static SendOtpFragment getInstance() {
        SendOtpFragment fragment = new SendOtpFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCountryIso(PhoneNumberHelper.getCurrentCountryISO(getContext()));
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .enableAutoManage(getActivity(), this)
                .addApi(Auth.CREDENTIALS_API)
                .build();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        /**
         * @{viewBinding} binding the fragment layout using data binding
         * */
        viewBinding = SendOtpBinding.inflate(inflater, container, false);
        viewModel = new ViewModelProvider(getActivity(), viewModelFactory).get(SendOtpViewModel.class);
        viewBinding.setViewModel(viewModel);
        phoneNumberEditView = viewBinding.viewEditPhoneNumber;
        phoneNumberEditViewInput = viewBinding.tilEditPhoneNumber;
        phoneNumberEditView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() <= String.valueOf(viewModel.getCountryCode()).length()) {
                    phoneNumberEditView.setText("+" + viewModel.getCountryCode());
                    phoneNumberEditView.setSelection(phoneNumberEditView.length());
                }
            }
        });
        setISOFormatOnPhoneField();
        viewModel.getSendOtpEvents().observe(getViewLifecycleOwner(), otpstates -> {
            switch (otpstates) {
                case PROCESS_PHONE_NUMBER:
                    viewModel.setPhoneNumber(phoneNumberEditView.getText().toString(), countryIso);
                    viewModel.validate();
                    break;
            }
        });

        return viewBinding.getRoot();
    }

    public void resetCountryFormat(Country selectedCountry) {
        setCountryIso(selectedCountry.getIso());
        viewModel.setCountryCode(selectedCountry.getCode());
        setISOFormatOnPhoneField();
    }

    /**
     * resetting and setting the text watcher for different country formats
     * */
    private void setISOFormatOnPhoneField() {
        PhoneNumberKit phoneNumberKit = new PhoneNumberKit(getActivity());
        phoneNumberKit.attachToInput(phoneNumberEditViewInput, viewModel.getCountryCode());
    }

    private void setCountryIso(String countryIso) {
        this.countryIso = countryIso;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.setCountryCode("+" + PhoneNumberHelper.getCurrentCountryCode(getContext()));
        waiting();
    }

    private void waiting() {
        Observable.interval(700, TimeUnit.MILLISECONDS)
                .take(700, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(() -> requestHint())
                .subscribe();
    }

    private void requestHint() {
        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();

        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(
                mGoogleApiClient, hintRequest);
        try {
            startIntentSenderForResult(intent.getIntentSender(),
                    RESOLVE_HINT, null, 0, 0, 0,null);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESOLVE_HINT) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                    phoneNumberEditView.setText(credential.getId());
                    viewModel.setPhoneNumber(credential.getId(), countryIso);
                }
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
