package com.nuyka.assignment.android.setup.contracts;

public interface NeedHelpContract {

    void onPhoneNumberUpdate();
    void onContactSupport();
}
