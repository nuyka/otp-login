package com.nuyka.assignment.android.setup.dependency.modules;

import android.content.Context;
import android.content.SharedPreferences;

import com.nuyka.assignment.android.BuildConfig;
import com.nuyka.assignment.android.setup.contracts.PreferenceWrapper;
import com.nuyka.assignment.android.setup.contracts.ResourceMapper;
import com.nuyka.assignment.android.setup.resimpl.PreferenceImpl;
import com.nuyka.assignment.android.setup.resimpl.ResourceImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Saurabh.
 */
@Module
public class ResourceModule {

    @Provides
    @Singleton
    SharedPreferences getSharedPreferences(Context context){
        return context.getSharedPreferences(BuildConfig.APPLICATION_ID + "_pref", Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    public PreferenceWrapper getAppPreferences(SharedPreferences preferences) {
        return new PreferenceImpl(preferences);
    }

    @Provides
    @Singleton
    public ResourceMapper resourceMapper(Context context) {
        return new ResourceImpl(context);
    }

}
