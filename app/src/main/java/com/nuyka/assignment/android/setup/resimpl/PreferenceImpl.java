package com.nuyka.assignment.android.setup.resimpl;

import android.content.SharedPreferences;

import com.nuyka.assignment.android.setup.contracts.PreferenceWrapper;

import javax.inject.Inject;


public class PreferenceImpl implements PreferenceWrapper {

    private static SharedPreferences.Editor editor;
    private static SharedPreferences sharedPreferences;

    @Inject
    public PreferenceImpl(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
        editor = sharedPreferences.edit();
    }

    @Override
    public void putInt(String key, int value) {
        editor.putInt(key,value).apply();
    }

    @Override
    public void putString(String key, String value) {
        editor.putString(key,value).apply();
    }

    @Override
    public void putFloat(String key, float value) {
        editor.putFloat(key,value).apply();
    }

    @Override
    public void putLong(String key, long value) {
        editor.putLong(key,value).apply();
    }

    @Override
    public void putBoolean(String key, boolean value) {
        editor.putBoolean(key,value).apply();
    }

    @Override
    public int getInt(String key, int defValue) {
        return sharedPreferences.getInt(key, defValue);
    }

    @Override
    public String getString(String key, String defValue) {
        return sharedPreferences.getString(key, defValue);
    }

    @Override
    public float getFloat(String key, float defValue) {
        return sharedPreferences.getFloat(key, defValue);
    }

    @Override
    public long getLong(String key, long defValue) {
        return sharedPreferences.getLong(key, defValue);
    }

    @Override
    public boolean getBoolean(String key, boolean defValue) {
        return sharedPreferences.getBoolean(key, defValue);
    }

    @Override
    public void clear() {
        editor.clear().apply();
    }
}
