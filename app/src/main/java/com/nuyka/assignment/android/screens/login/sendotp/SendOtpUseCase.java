package com.nuyka.assignment.android.screens.login.sendotp;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.nuyka.assignment.android.setup.event.SingleLiveEvent;
import com.nuyka.assignment.android.utils.Constants;

import javax.inject.Inject;

/**
 * Created by Saurabh.
 */
public class SendOtpUseCase {

    private final String TAG = SendOtpUseCase.class.getName();

    private String phoneNumber;
    private FirebaseAuth mAuth;
    private PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    private String countryISO;
    private Phonenumber.PhoneNumber phoneNumberProto;

    private SingleLiveEvent<Constants.STAGES> otpUserCaseEvents = new SingleLiveEvent();

    @Inject
    public SendOtpUseCase() {
        mAuth = FirebaseAuth.getInstance();
    }

    public void setPhoneNumber(String number) {
        this.phoneNumber = number;
        try {
            phoneNumberProto = phoneUtil.parse(phoneNumber, countryISO);
        } catch (NumberParseException e) {
            Log.e(TAG, "NumberParseException was thrown: " + e.toString());
        }
    }

    public boolean isMobileNumberValid() {
        return phoneUtil.isValidNumber(phoneNumberProto);
    }

    public SingleLiveEvent<Constants.STAGES> getOtpUserCaseEvents() {
        return otpUserCaseEvents;
    }

    public String getPhoneNumber() {
        return phoneNumberProto == null? "" : phoneUtil.format(phoneNumberProto, PhoneNumberUtil.PhoneNumberFormat.E164);
    }

    public void setCountryISO(String countryISO) {
        this.countryISO = countryISO;
    }
}
