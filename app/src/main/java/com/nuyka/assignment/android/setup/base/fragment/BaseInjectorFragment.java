package com.nuyka.assignment.android.setup.base.fragment;

import android.widget.Toast;

import com.nuyka.assignment.android.setup.contracts.ToastWrapper;
import com.nuyka.assignment.android.setup.factory.ViewModelFactory;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import dagger.android.support.DaggerFragment;

public abstract class BaseInjectorFragment extends DaggerFragment implements ToastWrapper {

    @Nullable
    @Inject
    protected
    ViewModelFactory viewModelFactory;

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}