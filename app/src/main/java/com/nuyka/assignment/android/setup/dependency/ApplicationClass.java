package com.nuyka.assignment.android.setup.dependency;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class ApplicationClass extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }

}