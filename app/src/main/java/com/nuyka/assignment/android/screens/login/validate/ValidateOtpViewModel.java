package com.nuyka.assignment.android.screens.login.validate;

import android.util.Log;

import com.google.firebase.auth.PhoneAuthProvider;
import com.nuyka.assignment.android.R;
import com.nuyka.assignment.android.screens.views.otpview.OnOtpCompletionListener;
import com.nuyka.assignment.android.setup.contracts.NeedHelpContract;
import com.nuyka.assignment.android.setup.contracts.ResourceMapper;
import com.nuyka.assignment.android.setup.event.SingleLiveEvent;
import com.nuyka.assignment.android.utils.Constants;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.lifecycle.ViewModel;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;

import static com.nuyka.assignment.android.utils.Constants.STAGES.CHANGE_NUMBER;
import static com.nuyka.assignment.android.utils.Constants.STAGES.SEND_EMAIL;

public class ValidateOtpViewModel extends ViewModel {

    private final String TAG = ValidateOtpViewModel.class.getName();

    public final ObservableBoolean otpEnableField = new ObservableBoolean(true);
    public final ObservableBoolean loadingStateVisibilityField = new ObservableBoolean(false);
    public final ObservableBoolean resendClickabiltity = new ObservableBoolean(false);
    public final ObservableBoolean errorOtpVisibility = new ObservableBoolean(false);
    public final ObservableField<String> resendViewTextField = new ObservableField<>();

    private SingleLiveEvent<Constants.STAGES> validateOtpEvents = new SingleLiveEvent();

    private ValidateOtpUseCase otpUseCase;
    private Disposable resendTimerDisposable;
    private ResourceMapper resourceMapper;

    @Inject
    public ValidateOtpViewModel(ValidateOtpUseCase useCase, ResourceMapper resourceMapper) {
        otpUseCase = useCase;
        this.resourceMapper = resourceMapper;
    }

    public void initialize() {
        startTimer();
    }

    public void onValidateOTPClick() {
        Log.e(TAG,"onValidateOTPClick");
        if (otpUseCase.isOtpValid()) {
            errorOtpVisibility.set(false);
            loadingStateVisibilityField.set(true);
            otpEnableField.set(false);
            validateOtpEvents.postValue(Constants.STAGES.OTP_TO_BE_VALIDATE);
        } else {
            errorOtpVisibility.set(true);
            if (otpUseCase.isRetryAvailable())
                otpUseCase.minusRetry();
            else
                validateOtpEvents.postValue(Constants.STAGES.SHOW_HELP);
        }
    }

    public void onNeedHelpClick() {
        Log.e(TAG,"onTroubleShootingClick");
        validateOtpEvents.postValue(Constants.STAGES.SHOW_HELP);
    }

    public void onResentOTPClick() {
        Log.e(TAG,"onResentOTPClick");
        resendClickabiltity.set(false);
        errorOtpVisibility.set(false);
        resendViewTextField.set(resourceMapper.getString(R.string.sending));
        validateOtpEvents.postValue(Constants.STAGES.OTP_RESEND);
    }

    public OnOtpCompletionListener getOtpCompletionListener() {
        return otpCompletionListener;
    }

    private OnOtpCompletionListener otpCompletionListener = new OnOtpCompletionListener() {

        @Override
        public void onOtpCompleted(String otp) {
            Log.e(TAG, "otp: " + otp);
            otpUseCase.setOtp(otp);
        }

        @Override
        public void onOtpIncomplete() {
            Log.e(TAG, "otp incomplete");
        }
    };

    public SingleLiveEvent<Constants.STAGES> getValidateOtpEvents() {
        return validateOtpEvents;
    }

    public String getCode() {
        return otpUseCase.getOtp();
    }

    public String getVerificationId() {
        return otpUseCase.getVerificationId();
    }

    public void setVerificationId(String verificationId) {
        otpUseCase.setVerificationId(verificationId);
    }

    public void setResendToken(PhoneAuthProvider.ForceResendingToken forceResendingToken) {
        otpUseCase.setResendToken(forceResendingToken);
    }

    public PhoneAuthProvider.ForceResendingToken getToken() {
        return otpUseCase.getResendToken();
    }

    public void otpValidated() {
        stopTimer();
        validateOtpEvents.postValue(Constants.STAGES.OTP_VALID);
    }

    public void otpInvalid() {
        errorOtpVisibility.set(true);
        loadingStateVisibilityField.set(false);
        otpEnableField.set(true);
        if (otpUseCase.isRetryAvailable())
            otpUseCase.minusRetry();
        else
            validateOtpEvents.postValue(Constants.STAGES.SHOW_HELP);
    }

    private void startTimer() {
        stopTimer();
        Log.e(TAG, "timer started");
        resendClickabiltity.set(false);
        resendTimerDisposable = Observable.interval(1, TimeUnit.SECONDS)
                .take(30, TimeUnit.SECONDS)
                .map(seconds -> {
                    Log.e("timer",seconds + "s");
                    resendViewTextField.set("00:" + (30 - seconds - 1));
                    return 0;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete(() -> {
                    Log.e("timer","over");
                    resendViewTextField.set(resourceMapper.getString(R.string.resend));
                    resendClickabiltity.set(true);
                })
                .subscribe();
    }

    private void stopTimer() {
        if (resendTimerDisposable != null) {
            Log.e(TAG, "timer stopped");
            resendTimerDisposable.dispose();
        }
    }

    public void otpResent() {
        Log.e(TAG,"otpResent");
        startTimer();
    }

    public void reset() {
        otpEnableField.set(true);
        loadingStateVisibilityField.set(false);
        errorOtpVisibility.set(false);
        resendViewTextField.set("");
    }

    public NeedHelpContract getHelpContract() {
        return needHelpContract;
    }

    private NeedHelpContract needHelpContract = new NeedHelpContract() {
        @Override
        public void onPhoneNumberUpdate() {
            stopTimer();
            validateOtpEvents.postValue(CHANGE_NUMBER);
        }

        @Override
        public void onContactSupport() {
            validateOtpEvents.postValue(SEND_EMAIL);
        }
    };

    public void otpSendingFailed() {
        resendViewTextField.set(resourceMapper.getString(R.string.resend));
        resendClickabiltity.set(true);
    }
}
