package com.nuyka.assignment.android.setup.contracts;

/**
 * Created by Saurabh.
 */
public interface ToastWrapper {

    void showMessage(String message);
}
