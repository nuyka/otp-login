package com.nuyka.assignment.android.screens.country;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.nuyka.assignment.android.R;
import com.nuyka.assignment.android.model.Country;
import com.nuyka.assignment.android.setup.base.activity.BaseInjectorActivity;

import androidx.lifecycle.ViewModelProvider;

public class CountrySearchActivity extends BaseInjectorActivity {

    private static final String TAG = CountrySearchActivity.class.getName();

    private CountrySearchViewModel viewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this, viewModelFactory).get(CountrySearchViewModel.class);
        registerViews();
        registerSearchEvents();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_countries, CountrySearchFragment.getInstance(), CountrySearchFragment.class.getName())
                .commit();
    }

    private void registerViews() {
        findViewById(R.id.view_image_back_arrow).setOnClickListener(v -> onBackPressed());
    }

    private void registerSearchEvents() {
        viewModel.getSearchEvents().observe(this, object -> {
            if (object instanceof Country) {
                final Country country = (Country) object;
                Log.e(TAG, "country: " + country);
                Intent sendIntent = new Intent();
                sendIntent.putExtra("country", country);
                setResult(RESULT_OK, sendIntent);
                onBackPressed();
            }
        });
    }

    @Override
    protected int setActivityLayout() {
        return R.layout.countries;
    }
}
