package com.nuyka.assignment.android.setup.dependency;

import com.nuyka.assignment.android.screens.country.CountrySearchActivity;
import com.nuyka.assignment.android.screens.country.CountrySearchFragment;
import com.nuyka.assignment.android.screens.home.HomeActivity;
import com.nuyka.assignment.android.screens.login.LoginActivity;
import com.nuyka.assignment.android.screens.login.sendotp.SendOtpFragment;
import com.nuyka.assignment.android.screens.login.validate.ValidateOtpFragment;
import com.nuyka.assignment.android.screens.splash.SplashActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Saurabh.
 */

@Module
public abstract class ViewBuilder {

    /**
     *
     * Activity builder
     * */

    @ContributesAndroidInjector
    abstract SplashActivity splashActivity();

    @ContributesAndroidInjector
    abstract LoginActivity loginActivity();

    @ContributesAndroidInjector
    abstract HomeActivity homeActivity();

    @ContributesAndroidInjector
    abstract CountrySearchActivity countrySearchActivity();

    /**
     *
     * Fragment builder
     * */

    @ContributesAndroidInjector
    abstract SendOtpFragment sendOtpFragment();

    @ContributesAndroidInjector
    abstract ValidateOtpFragment validateOtpFragment();

    @ContributesAndroidInjector
    abstract CountrySearchFragment countrySearchFragment();
}