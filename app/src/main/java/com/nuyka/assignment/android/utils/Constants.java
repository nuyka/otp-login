package com.nuyka.assignment.android.utils;

public class Constants {

    public static final String IS_USER_LOGIN = "is_user_logged_in";
    public static final int SEND_OTP_REQUEST = 101;
    public static final int RESENT_OTP_REQUEST = 102;
    public static final String MAIL_TO = "saurabh30pant@gmail.com";
    public static final String SUBJECT = "Need help!";

    public enum STAGES {
        OTP_SENT,
        OTP_SENDING_FAILED,
        OTP_TO_BE_SENT,
        OTP_TO_BE_VALIDATE,
        OTP_RESEND,
        MOBILE_INCORRECT,
        OTP_VALID,
        OTP_INVALID,
        CHANGE_NUMBER,
        PROCESS_PHONE_NUMBER,
        SHOW_HELP,
        SEND_EMAIL,
        SELECT_CODE
    }

    public enum SEARCH_STATES {
        CLEAR_SEARCH
    }
}
