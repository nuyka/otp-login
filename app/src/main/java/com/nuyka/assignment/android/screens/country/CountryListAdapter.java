package com.nuyka.assignment.android.screens.country;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.nuyka.assignment.android.databinding.ListItemCountrySearchBinding;
import com.nuyka.assignment.android.model.Country;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CountryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Country> countries = new ArrayList<>();
    private CountrySearchViewModel viewModel;

    public CountryListAdapter(CountrySearchViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListItemCountrySearchBinding itemBinding = ListItemCountrySearchBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ViewHolder holder = (ViewHolder) viewHolder;
        holder.itemBinding.setCountry(countries.get(position));
        holder.itemBinding.setViewModel(viewModel);
        holder.itemBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return null == countries ? 0 : countries.size();
    }

    public void updateCountries(List<Country> countries) {
        this.countries = countries;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final ListItemCountrySearchBinding itemBinding;

        public ViewHolder(ListItemCountrySearchBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding = itemBinding;
        }
    }
}