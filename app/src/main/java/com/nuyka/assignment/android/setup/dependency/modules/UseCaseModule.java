package com.nuyka.assignment.android.setup.dependency.modules;

import com.nuyka.assignment.android.screens.login.sendotp.SendOtpUseCase;
import com.nuyka.assignment.android.screens.login.validate.ValidateOtpUseCase;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Saurabh.
 */
@Module
public class UseCaseModule {

    @Provides
    public SendOtpUseCase sendOtpUseCase() {
        return new SendOtpUseCase();
    }

    @Provides
    public ValidateOtpUseCase validateOtpUseCase() {
        return new ValidateOtpUseCase();
    }
}
