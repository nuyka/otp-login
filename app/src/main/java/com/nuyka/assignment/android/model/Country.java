package com.nuyka.assignment.android.model;

import java.io.Serializable;

public class Country implements Serializable {

    private String iso;
    private String name;
    private String code;

    public Country(String iso, String name, String code) {
        this.iso = iso;
        this.name = name;
        this.code = code;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s", name, iso, code);
    }
}
