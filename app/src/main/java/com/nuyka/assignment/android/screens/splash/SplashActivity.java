package com.nuyka.assignment.android.screens.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.nuyka.assignment.android.R;
import com.nuyka.assignment.android.screens.home.HomeActivity;
import com.nuyka.assignment.android.screens.login.LoginActivity;
import com.nuyka.assignment.android.setup.base.activity.BaseInjectorActivity;
import com.nuyka.assignment.android.setup.contracts.PreferenceWrapper;
import com.nuyka.assignment.android.setup.contracts.ResourceMapper;

import javax.inject.Inject;

import androidx.core.app.ActivityOptionsCompat;

import static com.nuyka.assignment.android.utils.Constants.IS_USER_LOGIN;


public class SplashActivity extends BaseInjectorActivity {

    private final String TAG = SplashActivity.class.getName();
    private final int HOME_SCREEN_DELAY = 1000;
    private final int LOGIN_SCREEN_DELAY = 2000;

    @Inject
    PreferenceWrapper preferenceWrapper;

    @Inject
    ResourceMapper resourceMapper;

    private ImageView viewAppLogoImage;
    private Animation fadeInAnim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewAppLogoImage = findViewById(R.id.view_image_app_logo);
        fadeInAnim = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        fadeInAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (preferenceWrapper.getBoolean(IS_USER_LOGIN, false)) {
                    Log.e(TAG, "user already logged in");
                    loadNextScreen(HomeActivity.class, HOME_SCREEN_DELAY);
                } else {
                    Log.e(TAG, "agent not logged in");
                    loadNextScreen(LoginActivity.class, LOGIN_SCREEN_DELAY);
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {}

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
    }

    private void loadNextScreen(Class activityName, long delay) {
        new Handler().postDelayed(() -> {
            ActivityOptionsCompat activityOptionsCompat =
                    ActivityOptionsCompat.makeSceneTransitionAnimation(SplashActivity.this,viewAppLogoImage,resourceMapper.getString(R.string.image_logo_transition));
            Intent in = new Intent(SplashActivity.this, activityName);
            startActivity(in,activityOptionsCompat.toBundle());
        }, delay);
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewAppLogoImage.startAnimation(fadeInAnim);
    }

    @Override
    protected int setActivityLayout() {
        return R.layout.splash;
    }
}