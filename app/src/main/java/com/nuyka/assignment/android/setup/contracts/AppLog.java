package com.nuyka.assignment.android.setup.contracts;

/**
 * Created by Saurabh.
 */
public interface AppLog {

    void logError(String message);
    void logDebug(String message);
}
