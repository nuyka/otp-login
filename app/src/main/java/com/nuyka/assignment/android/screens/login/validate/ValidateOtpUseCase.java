package com.nuyka.assignment.android.screens.login.validate;

import com.google.firebase.auth.PhoneAuthProvider;

import javax.inject.Inject;

/**
 * Created by Saurabh.
 */
public class ValidateOtpUseCase {

    private final String TAG = ValidateOtpUseCase.class.getName();
    private char[] otp = new char[6];
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private int retryCount = 3;
    /**
     * @implNote use case class maintains the request id required for its operation
     */
    @Inject
    public ValidateOtpUseCase() {}

    public boolean isOtpValid() {
        try {
            Integer.parseInt(String.valueOf(otp));
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public void setOtp(String code) {
        otp = code.toCharArray();
    }

    public void setVerificationId(String mVerificationId) {
        this.mVerificationId = mVerificationId;
    }

    public void setResendToken(PhoneAuthProvider.ForceResendingToken mResendToken) {
        this.mResendToken = mResendToken;
    }

    public String getOtp() {
        return String.valueOf(otp);
    }

    public String getVerificationId() {
        return mVerificationId;
    }

    public PhoneAuthProvider.ForceResendingToken getResendToken() {
        return mResendToken;
    }

    public void minusRetry() {
        retryCount -= 1;
    }

    public boolean isRetryAvailable() {
        return retryCount > 0;
    }
}
