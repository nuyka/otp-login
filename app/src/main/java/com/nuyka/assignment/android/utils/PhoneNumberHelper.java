package com.nuyka.assignment.android.utils;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.i18n.phonenumbers.PhoneNumberUtil;

public class PhoneNumberHelper {

    private static final String TAG = PhoneNumberHelper.class.getName();

    public static String getCurrentCountryISO(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String countryIso = telephonyManager.getSimCountryIso().toUpperCase();
        Log.e(TAG, "iso : " + countryIso);
        return countryIso;
    }

    public static int getCurrentCountryCode(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String countryIso = telephonyManager.getSimCountryIso().toUpperCase();
        Log.e(TAG, "iso : " + countryIso);
        return PhoneNumberUtil.getInstance().getCountryCodeForRegion(countryIso);
    }
}
