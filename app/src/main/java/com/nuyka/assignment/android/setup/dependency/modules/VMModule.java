package com.nuyka.assignment.android.setup.dependency.modules;

import com.nuyka.assignment.android.screens.country.CountrySearchViewModel;
import com.nuyka.assignment.android.screens.login.sendotp.SendOtpUseCase;
import com.nuyka.assignment.android.screens.login.sendotp.SendOtpViewModel;
import com.nuyka.assignment.android.screens.login.validate.ValidateOtpUseCase;
import com.nuyka.assignment.android.screens.login.validate.ValidateOtpViewModel;
import com.nuyka.assignment.android.setup.contracts.ResourceMapper;
import com.nuyka.assignment.android.setup.factory.ViewModelFactory;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;

import javax.inject.Provider;

import androidx.lifecycle.ViewModel;
import dagger.MapKey;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public class VMModule {

    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @MapKey
    public @interface ViewModelKey {
        Class<? extends ViewModel> value();
    }

    @Provides
    ViewModelFactory viewModelFactory(Map<Class<? extends ViewModel>, Provider<ViewModel>> providerMap) {
        return new ViewModelFactory(providerMap);
    }

    @Provides
    @IntoMap
    @ViewModelKey(SendOtpViewModel.class)
    ViewModel sendOtpViewModel(SendOtpUseCase otpUseCase) {
        return new SendOtpViewModel(otpUseCase);
    }

    @Provides
    @IntoMap
    @ViewModelKey(ValidateOtpViewModel.class)
    ViewModel validateOtpViewModel(ValidateOtpUseCase otpUseCase, ResourceMapper resourceMapper) {
        return new ValidateOtpViewModel(otpUseCase, resourceMapper);
    }

    @Provides
    @IntoMap
    @ViewModelKey(CountrySearchViewModel.class)
    ViewModel countrySearchViewModel() {
        return new CountrySearchViewModel();
    }
}