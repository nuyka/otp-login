package com.nuyka.assignment.android.screens.login.validate;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.nuyka.assignment.android.databinding.ValidateOtpBinding;
import com.nuyka.assignment.android.receivers.SmsBroadcastReceiver;
import com.nuyka.assignment.android.screens.dialogs.NeedHelpFragment;
import com.nuyka.assignment.android.screens.views.otpview.OtpView;
import com.nuyka.assignment.android.setup.base.fragment.BaseInjectorFragment;
import com.nuyka.assignment.android.setup.contracts.OtpReceivedInterface;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import static com.nuyka.assignment.android.utils.Constants.MAIL_TO;
import static com.nuyka.assignment.android.utils.Constants.SUBJECT;

public class ValidateOtpFragment extends BaseInjectorFragment implements OtpReceivedInterface {

    private final String TAG = ValidateOtpFragment.class.getName();

    private ValidateOtpViewModel viewModel;
    private ValidateOtpBinding viewBinding;
    private OtpView otpView;
    private SmsBroadcastReceiver mSmsBroadcastReceiver;

    public static ValidateOtpFragment getInstance() {
        ValidateOtpFragment fragment = new ValidateOtpFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSmsBroadcastReceiver = new SmsBroadcastReceiver();
        mSmsBroadcastReceiver.setOnOtpListeners(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
        getContext().registerReceiver(mSmsBroadcastReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getContext().unregisterReceiver(mSmsBroadcastReceiver);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        /**
         * @{viewBinding} binding the fragment layout using data binding
         * */
        viewBinding = ValidateOtpBinding.inflate(inflater, container, false);
        viewModel = new ViewModelProvider(getActivity(), viewModelFactory).get(ValidateOtpViewModel.class);
        viewBinding.setViewModel(viewModel);
        otpView = viewBinding.layoutOptView;
        otpView.setOtpCompletionListener(viewModel.getOtpCompletionListener());

        return viewBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.initialize();
        viewModel.getValidateOtpEvents().observe(getViewLifecycleOwner(), stages -> {
            switch (stages) {
                case SHOW_HELP:
                    // show help
                    NeedHelpFragment fragment = new NeedHelpFragment();
                    fragment.setNeedHelpContract(viewModel.getHelpContract());
                    fragment.show(getActivity().getSupportFragmentManager(), NeedHelpFragment.class.getName());
                    break;
                case SEND_EMAIL:
                    sendEmail(new String[]{MAIL_TO}, SUBJECT);
                    break;
            }
        });
    }

    private void sendEmail(String[] addresses, String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @Override
    public void onOtpReceived(String otp) {
        Log.e(TAG, "otp is " + otp);
        otpView.setText(otp);
        viewModel.onValidateOTPClick();
    }

    @Override
    public void onOtpTimeout() {}
}
