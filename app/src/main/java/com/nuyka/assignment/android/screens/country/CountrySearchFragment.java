package com.nuyka.assignment.android.screens.country;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.nuyka.assignment.android.databinding.CountrySearchBinding;
import com.nuyka.assignment.android.setup.base.fragment.BaseInjectorFragment;
import com.nuyka.assignment.android.utils.Constants;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.lifecycle.ViewModelProvider;

public class CountrySearchFragment extends BaseInjectorFragment {

    private final String TAG = CountrySearchFragment.class.getName();

    private CountrySearchBinding viewBinding;
    private CountryListAdapter listAdapter;
    private CountrySearchViewModel viewModel;
    private AppCompatEditText searchFieldView;

    public static CountrySearchFragment getInstance() {
        CountrySearchFragment fragment = new CountrySearchFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewModel = new ViewModelProvider(getActivity(), viewModelFactory).get(CountrySearchViewModel.class);
        /**
         * @{viewBinding} binding the fragment layout using data binding
         * */
        viewBinding = CountrySearchBinding.inflate(inflater, container, false);
        listAdapter = new CountryListAdapter(viewModel);
        viewBinding.setAdapter(listAdapter);
        viewBinding.setViewModel(viewModel);
        searchFieldView = viewBinding.viewEditCountry;
        searchFieldView.addTextChangedListener(viewModel.getTextChangeListener());
        viewModel.getSearchEvents().observe(getViewLifecycleOwner(), event -> {
            if (event instanceof Constants.SEARCH_STATES) {
                final Constants.SEARCH_STATES state = (Constants.SEARCH_STATES) event;
                switch (state) {
                    case CLEAR_SEARCH:
                        searchFieldView.setText("");
                        break;
                }
            }
        });
        return viewBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.init();
    }
}
