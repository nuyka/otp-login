package com.nuyka.assignment.android.utils;

import android.util.Log;
import android.widget.TextView;

import com.nuyka.assignment.android.model.Country;
import com.nuyka.assignment.android.screens.country.CountryListAdapter;

import java.util.List;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

public class BindingHelper {

    private static final String TAG = BindingHelper.class.getName();

    @BindingAdapter("items_country")
    public static void setCountries(RecyclerView recyclerView, List<Country> countries) {
        CountryListAdapter countryListAdapter = (CountryListAdapter) recyclerView.getAdapter();
        if (countryListAdapter != null) {
            Log.e(TAG, "country list size: " + countries.size());
            countryListAdapter.updateCountries(countries);
        }
    }

    @BindingAdapter("detail")
    public static void setCountryDetails(TextView textView, Country country) {
        textView.setText(String.format("%s (%s)", country.getName(), country.getIso()));
    }
}
