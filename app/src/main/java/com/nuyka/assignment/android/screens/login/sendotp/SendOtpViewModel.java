package com.nuyka.assignment.android.screens.login.sendotp;

import android.util.Log;

import com.nuyka.assignment.android.setup.event.SingleLiveEvent;
import com.nuyka.assignment.android.utils.Constants;

import javax.inject.Inject;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.lifecycle.ViewModel;

public class SendOtpViewModel extends ViewModel {

    private final String TAG = SendOtpViewModel.class.getName();

    public final ObservableField<String> countryCodeField = new ObservableField<>();
    public final ObservableBoolean mobileEnableField = new ObservableBoolean(true);
    public final ObservableBoolean loadingStateVisibilityField = new ObservableBoolean(false);
    public final ObservableBoolean errorNumberVisibility = new ObservableBoolean(false);

    private SingleLiveEvent<Constants.STAGES> sendOtpEvents = new SingleLiveEvent();

    private SendOtpUseCase otpUseCase;

    @Inject
    public SendOtpViewModel(SendOtpUseCase useCase) {
        otpUseCase = useCase;
        otpUseCase.getOtpUserCaseEvents().observeForever(otpUseCaseEventsListener);
    }

    public void onSendOtpClick() {
        Log.e(TAG,"onSendOtpClick");
        errorNumberVisibility.set(false);
        loadingStateVisibilityField.set(true);
        mobileEnableField.set(false);
        sendOtpEvents.postValue(Constants.STAGES.PROCESS_PHONE_NUMBER);
    }

    public void onCountryCodeClick() {
        Log.e(TAG,"onCountryCodeClick");
        sendOtpEvents.postValue(Constants.STAGES.SELECT_CODE);
    }

    public void setCountryCode(String countryCode) {
        countryCodeField.set(countryCode);
    }

    public int getCountryCode() {
        try {
            return Integer.parseInt(countryCodeField.get().substring(1));
        } catch (NumberFormatException | NullPointerException e) {
            return 91;
        }
    }

    private androidx.lifecycle.Observer<Constants.STAGES> otpUseCaseEventsListener
            = otpstate -> sendOtpEvents.postValue(otpstate);

    public SingleLiveEvent<Constants.STAGES> getSendOtpEvents() {
        return sendOtpEvents;
    }

    public String getPhoneNumber() {
        return otpUseCase.getPhoneNumber();
    }

    public void otpSent() {
        Log.e(TAG, "otp sent");
        loadingStateVisibilityField.set(false);
        mobileEnableField.set(true);
        sendOtpEvents.postValue(Constants.STAGES.OTP_SENT);
    }

    public void otpSendingFailed() {
        Log.e(TAG, "otp not sent");
        loadingStateVisibilityField.set(false);
        mobileEnableField.set(true);
    }

    public void setPhoneNumber(String phone, String countryIso) {
        otpUseCase.setCountryISO(countryIso);
        otpUseCase.setPhoneNumber(phone);
    }

    public void validate() {
        if(otpUseCase.isMobileNumberValid())
            sendOtpEvents.postValue(Constants.STAGES.OTP_TO_BE_SENT);
        else {
            loadingStateVisibilityField.set(false);
            mobileEnableField.set(true);
            errorNumberVisibility.set(true);
        }
    }
}
