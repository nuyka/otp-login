package com.nuyka.assignment.android.screens.dialogs;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.nuyka.assignment.android.R;
import com.nuyka.assignment.android.setup.contracts.NeedHelpContract;

public class NeedHelpFragment extends BottomSheetDialogFragment {

    private NeedHelpContract needHelpContract;

    public void setNeedHelpContract(NeedHelpContract needHelpContract) {
        this.needHelpContract = needHelpContract;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        View view = LayoutInflater.from(getContext()).inflate(R.layout.need_help, null);
        view.findViewById(R.id.view_change_phone).setOnClickListener(v -> {
            dismiss();
            if (needHelpContract != null)
                needHelpContract.onPhoneNumberUpdate();
        });
        view.findViewById(R.id.view_contact_support).setOnClickListener(v -> {
            dismiss();
            if (needHelpContract != null)
                needHelpContract.onContactSupport();
        });
        dialog.setContentView(view);
    }
}
