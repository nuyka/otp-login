package com.nuyka.assignment.android.screens.country;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;

import com.nuyka.assignment.android.model.Country;
import com.nuyka.assignment.android.setup.event.SingleLiveEvent;
import com.nuyka.assignment.android.utils.CountryCodes;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableList;
import androidx.lifecycle.ViewModel;

import static com.nuyka.assignment.android.utils.Constants.SEARCH_STATES.CLEAR_SEARCH;

public class CountrySearchViewModel extends ViewModel {

    private final String TAG = CountrySearchViewModel.class.getName();

    public final ObservableList<Country> countries = new ObservableArrayList<>();
    public final ObservableBoolean clearSearchVisibility = new ObservableBoolean(false);

    private SingleLiveEvent searchEvents = new SingleLiveEvent();

    public void init() {
        countries.addAll(CountryCodes.getCountries());
    }

    public void onCountrySelect(Country country) {
        Log.e(TAG,"onCountrySelect: " + country.getName());
        searchEvents.postValue(country);
    }

    public void onSearchClear() {
        Log.e(TAG,"onSearchClear");
        searchEvents.postValue(CLEAR_SEARCH);
    }

    public TextWatcher getTextChangeListener() {
        return textWatcher;
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void afterTextChanged(Editable text) {
            if (TextUtils.isEmpty(text.toString())) {
                clearSearchVisibility.set(false);
                countries.clear();
                init();
            } else {
                clearSearchVisibility.set(true);
                filterList(text.toString());
            }
        }
    };

    private void filterList(String searchStr) {
        countries.clear();
        for (Country country : CountryCodes.getCountries()) {
            if (country.getName().toLowerCase().contains(searchStr.toLowerCase()) ||
                    country.getCode().toLowerCase().contains(searchStr.toLowerCase()) ||
                    country.getIso().toLowerCase().contains(searchStr.toLowerCase())) {
                countries.add(country);
            }
        }
        if (countries.isEmpty())
            init();
    }

    public SingleLiveEvent getSearchEvents() {
        return searchEvents;
    }
}
